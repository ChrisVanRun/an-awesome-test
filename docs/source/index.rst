.. an-awesome-test documentation master file, created by
   sphinx-quickstart on Fri Dec 28 12:16:59 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to an-awesome-test's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

This is just a test. reStructuredText to make it future proof... jeeej!

This is some more testing. To add some more content and see what the changes are...

I wonder if this works:
:ref:`another-page-yo`

	Is this text correctly rendered as code?

Adding some text

.. code-block:: bash

	ls -al /home/derp


.. image:: derp.gif

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
